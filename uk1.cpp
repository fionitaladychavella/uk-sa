#include <iostream>
using namespace std;

int main() {
    // Inisialisasi jumlah koin palsu dan asli
    int jumlah_asli = 1000;
    int jumlah_palsu = 1;

    // Inisialisasi berat koin palsu dan asli
    double berat_asli = 5;
    double berat_palsu = 4.5;

    // Menghitung jumlah iterasi maksimal
    int iterasi_maksimal = jumlah_asli + jumlah_palsu - 1;

    // Algoritma decrease
    for (int i = iterasi_maksimal; i > 0; i--) {
        double berat_total = (jumlah_asli * berat_asli) + (jumlah_palsu * berat_palsu);
        double berat_rata = berat_total / i;

        if (berat_rata == berat_palsu) {
            cout << "Koin palsu ditemukan pada iterasi ke-" << iterasi_maksimal - i + 1 << endl;
            break;
        }
    }

    // Menampilkan hasil
    cout << "Jumlah koin asli: " << jumlah_asli << endl;
    cout << "Jumlah koin palsu: " << jumlah_palsu << endl;

    return 0;
}

